# Makefile for MacOSX Screenshot path changer.
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
GCC = gcc
GPP = g++
AR  = ar

# TARGET settings
TARGET_PKG = macscrnshotpath
TARGET_DIR = ./bin
TARGET_OBJ = ./obj

# Compiler optiops
COPTS += -std=c++11
COPTS += -ffast-math -fexceptions -O3

# FLTK configs
FCG = fltk-config --use-images
FLTKCFG_CXX := $(shell ${FCG} --cxxflags)
FLTKCFG_LFG := $(shell ${FCG} --ldflags)

# FLTK image toolkit
FLIMGTK = ../fl_imgtk/lib

# Sources
SRC_PATH = src
SRCS = $(wildcard $(SRC_PATH)/*.cpp)

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)

# CC FLAG
CFLAGS  = -I$(SRC_PATH)
CFLAGS += $(FLTKCFG_CXX)
CFLAGS += -I$(FLIMGTK)
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# LINK FLAG
#LFLAGS  = -static
LFLAGS += -L$(FLIMGTK)
LFLAGS += -lfl_imgtk
LFLAGS += -lpthread
LFLAGS += $(FLTKCFG_LFG)

.PHONY: prepare clean

all: prepare clean continue

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS)
	@echo "Generating $@ ..."
	@$(GPP) $(TARGET_OBJ)/*.o $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."

