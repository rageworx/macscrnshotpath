# MacOS X 사용자를 위한 스크린샷 위치 변경 앱

* 이 프로그램은 MacOS X 에서 기본으로 설정 되어 있는 스크린샷 위치를 간단히 변경하게 해 주는 앱 입니다.

## 지원되는 버젼

* 별 다른 문제가 없는 이상 64bit 의 MacOS X 를 모두 지원 합니다.
* 타국어 언어를 유일하게 지원하지 않습니다: 한글만 지원합니다.

## 원리

* 이 앱은 콘솔에서 작업 하는 것을 대신해서 관리 하는 GUI front-end 입니다.
* defaults read 를 통해서 스크린샷 위치를 얻고,
* defaults write 를 통해서 스크린샷 위치를 기록 하며,
* 덤으로 해당 위치에 있던 스크린샷 이미지를 모두 새 위치로 옮겨 줍니다.

## 마지막 변경 내역

### 버젼 0.1.0.0
* 현재 개발중 인 버젼.

## 기존 변경 사항 

## 소스 빌드 방법
* 먼저 'FLTK-1.3.5-2-ts'를 컴파일 하고 설치 해 둬야 합니다 (brew 가 필요 합니다)
* 'fl_imgtk' 를 다음으로 컴파일 해 두고 이 앱 위치와 동일한 디렉터리 위치에 둬야 합니다.
* 'make' 를 이용해서 컴파일 하면 됩니다.

## 라이센스
* FLTK-License, and LGPL.
