// Programmed by Raphael Kim.
//
#include <unistd.h>
#include <signal.h>
#include <dirent.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include <string>
#include <vector>
#include <array>
#include <algorithm>
#include <fstream>

#include <pthread.h>

#include <FL/x.H>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_PNG_Image.H>
#include "Fl_MulticolorBarGraph.H"

#include "fl_imgtk.h"
#include "version.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME         "MacOS X 스크린샷 위치 바꾸기"

/////////////////////////////////////////////////////////////////////////

static string               strAppTitle;

static Fl_Double_Window*    winMain = NULL;
static Fl_Box*              boxGraphs = NULL;
static MulticolorBarGraph*  barProgress = NULL;
static Fl_RGB_Image*        winIcon = NULL;
static Fl_Box*              boxCopyright = NULL;
static Fl_Output*           outPath = NULL;

static bool                 threadlived = true;
static pthread_t            pt = 0;

static string               pathScreenShot;

/////////////////////////////////////////////////////////////////////////

string exec(const char* cmd) 
{
    char buffer[1024] = {0};
    std::string result;

    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try 
	{
        while (fgets(buffer, sizeof buffer, pipe) != NULL) 
        {
            result += buffer;
        }
    } catch (...) 
    {
        pclose(pipe);
        throw;
    }

    pclose(pipe);
    
    return result;
}

vector< string> split_string(const string& str, const string& delimiter)
{
    vector< string > strings;

    string::size_type pos = 0;
    string::size_type prev = 0;

    while ( (pos = str.find(delimiter, prev) ) != string::npos )
    {
        strings.push_back(str.substr(prev, pos - prev));
        prev = pos + 1;
    }

    strings.push_back(str.substr(prev));

    return strings;
}

inline string& rtrim( string& s, const char* t = " \t\n\r\f\v" )
{
    s.erase(s.find_last_not_of(t) + 1);
    return s;
}

inline string& ltrim( string& s, const char* t = " \t\n\r\f\v" )
{
    s.erase(0, s.find_first_not_of(t));
    return s;
}

inline string& trim( string& s, const char* t = " \t\n\r\f\v" )
{
    return ltrim(rtrim(s, t), t);
}

Fl_RGB_Image* loadIcon( Fl_Double_Window* w )
{
    if ( w == NULL )
        return NULL;

    string trypath = "/usr/local/share/icons/hicolor/256x256/apps";
    string fpath = trypath + "/rkcpumon.png";
    bool   existed = false;

    if ( access( fpath.c_str() , 0 ) == 0 )
    {
        existed = true;
    }
    else
    {
        trypath = getenv( "PWD" );
        fpath = trypath + "/res/rkcpumon.png";

        if ( access( fpath.c_str(), 0 ) == 0 )
        {
            existed = true;
        }
    }

    if ( existed == true )
    {
        Fl_RGB_Image* imgIcon = new Fl_PNG_Image( fpath.c_str() );
        if ( imgIcon != NULL )
        {
            w->icon( imgIcon );
            return imgIcon;
        }
    }

    return NULL;
}

void generateComponents()
{
    unsigned colBg = 0x333333FF;
    unsigned colFg = 0xAAAAAAFF;
    unsigned lblSz = 12;

    unsigned ww = 500;
    unsigned wh = 350;

#ifdef APPTITLE
    strAppTitle = APPTITLE;
#else
    strAppTitle  = DEF_APP_NAME;
#endif
    strAppTitle += " version ";
    strAppTitle += DEF_APP_VERSION;

    winMain = new Fl_Double_Window( ww, wh, strAppTitle.c_str() );
    if ( winMain != NULL )
    {
        winIcon = loadIcon( winMain );
        winMain->color( colBg );
        winMain->labelfont( FL_HELVETICA );
        winMain->labelcolor( colFg );
        winMain->labelsize( lblSz );

        int putx = 100;
        int puty = 10;
        int putw = winMain->w() - putx - 5;
        int puth = 30;

        outPath = new Fl_Output( putx, puty, putw, puth, "저장위치: " );
        if ( outPath != NULL )
        {
            outPath->color( fl_darker( winMain->color() ) );
            outPath->labelfont( winMain->labelfont() );
            outPath->labelcolor( winMain->labelcolor() );
            outPath->labelsize( winMain->labelsize() );
            outPath->textfont( FL_COURIER );
            outPath->textsize( winMain->labelsize() );
            outPath->textcolor( 0xFFFFFF00 );

            puty += puth + 5;
        }

        puty = wh - puth - 5;

        boxCopyright = new Fl_Box( putx, puty, putw, puth,
                                   "(C) 2020 rageworx@@gmail.com" );
        if ( boxCopyright != NULL )
        {
            boxCopyright->align( FL_ALIGN_INSIDE | FL_ALIGN_RIGHT );
            boxCopyright->labelfont( winMain->labelfont() );
            boxCopyright->labelsize( winMain->labelsize() );
            boxCopyright->labelcolor( winMain->labelcolor() );
        }

        winMain->end();
        winMain->show();
    }
}

bool readScreenShotPath()
{
    string reads = exec( "defaults read com.apple.screencapture" );

    if ( reads.size() > 0 )
    {
        vector< string > readln = split_string( reads, "\n" );

        for ( unsigned cnt=0; cnt<readln.size(); cnt++ )
        {
            size_t fpos = readln[cnt].find( "location = " );
            if ( fpos != string::npos )
            {
                size_t fposQ1 = readln[cnt].find( "\"" );
                if ( fposQ1 != string::npos )
                {
                    size_t fposQ2 = readln[cnt].find( "\"", fposQ1 + 1 );

                    if ( fposQ2 != string::npos )
                    {
                        size_t csz1 = fposQ1 + 1;
                        size_t csz2 = fposQ2 - fposQ1 - 1;
                        pathScreenShot = readln[cnt].substr( csz1, csz2 );
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

void* threadRun( void* p )
{
    if ( readScreenShotPath() == true )
    {
        outPath->value( pathScreenShot.c_str() );
    }

    pthread_exit( 0 );
    return NULL;
}

void sighandler( int sig )
{

}

int main( int argc, char** argv )
{
    signal( SIGINT, sighandler );

    Fl::scheme( "flat" );
    Fl::lock();

    generateComponents();

    pthread_create( &pt, NULL, threadRun, NULL );

    int reti = Fl::run();

    threadlived = false;
    pthread_cancel( pt );
    pthread_join( pt, 0 );

    fl_imgtk::discard_user_rgb_image( winIcon );

    return reti;
}
